<?php
/** 
 * As configurações básicas do WordPress.
 *
 * Esse arquivo contém as seguintes configurações: configurações de MySQL, Prefixo de Tabelas,
 * Chaves secretas, Idioma do WordPress, e ABSPATH. Você pode encontrar mais informações
 * visitando {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. Você pode obter as configurações de MySQL de seu servidor de hospedagem.
 *
 * Esse arquivo é usado pelo script ed criação wp-config.php durante a
 * instalação. Você não precisa usar o site, você pode apenas salvar esse arquivo
 * como "wp-config.php" e preencher os valores.
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar essas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'zero84');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', 'root');

/** nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Conjunto de caracteres do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Você pode alterá-las a qualquer momento para desvalidar quaisquer cookies existentes. Isto irá forçar todos os usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'd];MJ-;|k|K>#{kB]K=p;C{)e7B(fx7e%&OVK|Jj;cD<5[(U3[$kb?eyXjmg8p^Z');
define('SECURE_AUTH_KEY',  '+An-m.4W6y5uJ-5?By{B+0h[X+Pyo<y]rJMMnd$5id@w_<r74RQnkRH+I(r}9::@');
define('LOGGED_IN_KEY',    '%|, PJ ++]%L#]DYsGl!i?GN_.P)tT]-^(ZUnX4F`xe+A/Tu8a1{EoHe6)htx-$t');
define('NONCE_KEY',        'rrd{+n]SDpplg$>wcmf-WZ<hk+]3O <LH!&r..hK^^C+E>=l|rrz`bjwk8I1^fPV');
define('AUTH_SALT',        ']aWIoo`XGx-}ZU Og`TL%}+`t&-kV}E@`,!S_}aP3}MXTT5%xcSIJ|z`a si}`a#');
define('SECURE_AUTH_SALT', 'B_>gs?t[I+[G7l|:ci`m)O5xDqGp)u+r(l7dhFX?Ifb7o+2qynn{3Lxo~jj~+{-~');
define('LOGGED_IN_SALT',   'a(~|8).+g/m7vC +}u%5wJT?16*10_h36SbnuK|sZmgyebzN +zqV(UFFI`OIZ4}');
define('NONCE_SALT',       '|*tFUA !.OKz8so|b))I%X,^sr[OYrHnsa|IGJ9KrUkM/7&jj+7:LC[Vc4t+4Zyr');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der para cada um um único
 * prefixo. Somente números, letras e sublinhados!
 */
$table_prefix  = 'wp_';


/**
 * Para desenvolvedores: Modo debugging WordPress.
 *
 * altere isto para true para ativar a exibição de avisos durante o desenvolvimento.
 * é altamente recomendável que os desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');
	
/** Configura as variáveis do WordPress e arquivos inclusos. */
require_once(ABSPATH . 'wp-settings.php');
